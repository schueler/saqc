.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. dios documentation master file, created by
   sphinx-quickstart on Sun Apr 19 02:36:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dios Docs
=========

.. currentmodule:: dios

The whole package :mod:`dios` is mainly a container for
the class :class:`dios.DictOfSeries`. See

.. toctree::

   dios.DictOfSeries <_api/dios.DictOfSeries>

.. toctree::
   :hidden:

   Repository <https://git.ufz.de/rdm/dios>
   example DictOfSeries <_api/dios.example_DictOfSeries>


Most magic happen in getting and setting elements.
To select any combination from columns and rows,
read the documentation about indexing:

.. toctree::

   doc_indexing

.. toctree::

   doc_cookbook

For the idea behind the Itype concept and its usage read:

.. toctree::

   doc_itype

For implemented methods and module functions,
respectively the full module api, see:

.. toctree::
   :maxdepth: 2

   dios_api

or browse the Index..

.. toctree::

   genindex

