.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

drift
=====



.. currentmodule:: saqc

.. autosummary::

   ~SaQC.flagDriftFromNorm
   ~SaQC.flagDriftFromReference
   ~SaQC.correctDrift
   ~SaQC.correctRegimeAnomaly
   ~SaQC.correctOffset
   ~SaQC.flagRegimeAnomaly
   ~SaQC.assignRegimeAnomaly
