.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

outliers
========



.. currentmodule:: saqc

.. autosummary::

   ~SaQC.flagByStray
   ~SaQC.flagMVScores
   ~SaQC.flagRaise
   ~SaQC.flagMAD
   ~SaQC.flagOffset
   ~SaQC.flagByGrubbs
   ~SaQC.flagRange
   ~SaQC.flagCrossStatistics
