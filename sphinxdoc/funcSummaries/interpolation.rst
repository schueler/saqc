.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

interpolation
=============



.. currentmodule:: saqc

.. autosummary::

   ~SaQC.interpolateByRolling
   ~SaQC.interpolateInvalid
   ~SaQC.interpolateIndex
