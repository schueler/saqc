.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

residues
========



.. currentmodule:: saqc

.. autosummary::

   ~SaQC.calculatePolynomialResidues
   ~SaQC.calculateRollingResidues
