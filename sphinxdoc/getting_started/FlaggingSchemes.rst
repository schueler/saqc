.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

DMP flagging scheme
===================

Possible flags
--------------

The DMP scheme produces the following flag constants:


* "ok"
* "doubtfull"
* "bad"
