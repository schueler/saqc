.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. include:: misc/title.rst


.. toctree::
   :hidden:
   :maxdepth: 1

   Repository <https://git.ufz.de/rdm-software/saqc>

Getting Started
===============

.. toctree::
   :caption: Getting started
   :maxdepth: 1
   :hidden:

   getting_started/InstallationGuide
   getting_started/TutorialAPI
   getting_started/TutorialCLI


.. toctree::
   :caption: Cookbooks
   :maxdepth: 1
   :hidden:

   cook_books/DataRegularisation
   cook_books/OutlierDetection
   cook_books/MultivariateFlagging

.. toctree::
   :caption: Documentation
   :maxdepth: 1
   :hidden:

   documentation/FlaggingTranslation
   documentation/GenericFunctions
   documentation/SourceTarget


.. toctree::
   :caption: Functions
   :hidden:
   :maxdepth: 1
   :glob:
   :titlesonly:

   resampling <funcSummaries/resampling>
   generic <funcSummaries/generic>
   outliers <funcSummaries/outliers>
   breaks <funcSummaries/breaks>
   constants <funcSummaries/constants>
   changepoints <funcSummaries/changepoints>
   drift <funcSummaries/drift>
   curvefit <funcSummaries/curvefit>
   interpolation <funcSummaries/interpolation>
   residues <funcSummaries/residues>
   tools <funcSummaries/tools>
   flagtools <funcSummaries/flagtools>
   rolling <funcSummaries/rolling>
   scores <funcSummaries/scores>
   transformation <funcSummaries/transformation>
   noise <funcSummaries/noise>
   pattern <funcSummaries/pattern>


.. toctree::
   :caption: SaQC
   :hidden:
   :maxdepth: 2

   Package <moduleAPIs/saqc>
   Config Environment <environment/configEnv>


.. toctree::
   :caption: Developer Resources
   :hidden:
   :maxdepth: 1

   Documentation Guide <how_to_doc/HowToDoc>
   Writing Functions <documentation/WritingFunctions>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

