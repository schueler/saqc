.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Config file Environment
=======================

.. list-table::
   :header-rows: 1

   * - Env-Value
     - Env-Doc
   * - "NAN"
     -  Pointwise absolute Value Function.
   * - "abs"
     -  Maximum Value Function. Ignores NaN.
   * - "max"
     -  Minimum Value Function. Ignores NaN.
   * - "min"
     -  Mean Value Function. Ignores NaN.
   * - "mean"
     -  Summation. Ignores NaN.
   * - "sum"
     -  Standart Deviation. Ignores NaN.
   * - "len"
     -  Pointwise Exponential.
   * - "exp"
     -  Pointwise Logarithm.
   * - "log"
     -  Logarithm, returning NaN for zero input, instead of -inf.
   * - "nanLog"
     -  Standart Deviation. Ignores NaN.
   * - "std"
     -  Variance. Ignores NaN.
   * - "var"
     -  Median. Ignores NaN.
   * - "median"
     -  Count Number of values. Ignores NaNs.
   * - "count"
     -  Identity.
   * - "id"
     -  Returns a Series` diff.
   * - "diff"
     -  Scales data to [0,1] Interval.
   * - "scale"
     -  Standardize with Standart Deviation.
   * - "zScore"
     -  Standardize with Median and MAD.
   * - "madScore"
     -  Standardize with Median and inter quantile range.
   * - "iqsScore"
     - 
   * - "GOOD"
     - 
   * - "BAD"
     - 
   * - "UNFLAGGED"
     - 
   * - "DOUBTFUL"
     - 
