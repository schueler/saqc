# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: GPL-3.0-or-later

recommonmark
sphinx
sphinx-automodapi
sphinxcontrib-fulltoc
sphinx-markdown-tables
m2r
jupyter-sphinx
